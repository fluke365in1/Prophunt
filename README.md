# Prop Hunt 
version 3

This project started as a private project, but I have decided to share most of it with you.
This project should be seen as a reboot of Prop Hunt on CS:GO. 
It's still an experimental mod, almost all features have been rewritten.

# New Features!

- Improved hider hitbox hit detection
  - Predicted hits ( TraceRay, BBoX line intersection)
- Fixed shadows
- Ingame admin menu to edit hider models
- Weapon balancing by weapon type
- A lot more options for hider models
  - Side/Back/Front position fix
  - 3 axis rotation
  - RGB Color, Skin
  - Health, Gravity & Speed
  - Chance of appearence inside the model selection menu
  - Add to map ( permanent or random )
- Hider & Seeker shop for skills
  - Round points system
  - Morph
  - Weapons
  - Custom items
- Autofreeze
- Taunt sound packs
- Add models to maps with random appearence
- For developers: A bunch a natives and forwards to write addons
- and much more

# Installation

### Step 1
- Install [Soundlib](https://forums.alliedmods.net/showthread.php?t=105816)
- Install [SpeedRules](https://gitlab.com/Zipcore/speedrules)

### Step 2
- Upload ```gamemode_casual_server.cfg``` & ```prophunt.cfg``` to your ```csgo/cfg``` folder.
- Add ```exec prophunt.cfg``` to your ```server.cfg```.

### Step 3 
- Upload the included ```configs/```, ```plugins/``` & ```translations/```folder.
- Restart your server or change map.

### Step 4
- Check if there are any errors by checking ```addons/sourcemod/logs```
- Open ```cfg/sourcemod``` and edit the new generated prophunt config to your needs.

# Adding taunt sounds

- Open ```taunt_packs.cfg``` which can be found under ```addons/sourcemod/configs/prophunt/```
- Start a new line like this to add a new soundpack: ```Pack:Pack I```, where ```Pack I``` is the name which will be displayed ingame. Optionally you can use ```VIP-Pack:Pack I``` to create a pack for VIP's only.
- Just add new soundfiles unter the new pack line, but don't forget to upload them, not only to your fastdl server, the gameserver needs the files aswell to be able to get their sound length.

# Adding models to a new map
- Join a server running the desired map.
- Type ```listmodels``` into console to get a HUGE list of models which are precached on the current map.
- Copy them over to an editor like Notepad++
- Sort out the ones you obviouly don't need: stickers, weapons, playermodels, etc.
- Goto ```addons\sourcemod\configs\prophunt\maps```.
- Create a new config like the others and add the models from your list
- Name, skin, color, position, rotation and more can be edited ingame later.
- Upload the config to your server and switch to the new map.
- Use ```ph_models``` in console as admin and edit your map config.
- And don't worry the ingame editor backups each time you edit something.

# Addons

Planned: More weapons, special weapons, cluster grenades, ... request stuff?

Free
- [Prophunt - Antighosting](https://gitlab.com/Zipcore/Prophunt-Antighosting) Tools to prevent players from revealing hider positions.
- [Prophunt - Observer Rules](https://gitlab.com/Zipcore/Prophunt-ObserverRules) Only allow seekers being spectated.
- [Prophunt - Taunt Grenade](https://gitlab.com/Zipcore/Prophunt-TauntGrenade) Allows seekers to use tagrenades as force taunt grenades.

Private ( Partners only for now, but may be released later )
- [Prophunt - ClusterGrenade](https://gitlab.com/Zipcore/Prophunt-ClusterGrenade) Allows seekers to upgrade grenades to cluster grenades, throws 3 additional grenades.
- [Prophunt - Darkness](https://gitlab.com/Zipcore/Prophunt-Darkness) Adds a seeker shop item, which allows them fight darknesss back which increases while the round is proceeeding.
- [Prophunt - Decoy](https://gitlab.com/Zipcore/Prophunt-Decoy) Adds a hider shop item, which allows them to place an explosive copy of their current model.
- [Prophunt - Fear](https://gitlab.com/Zipcore/Prophunt-Fear) Transforms seeker HP into fear, which regenerates over time.
- [Prophunt - Pentagram](https://gitlab.com/Zipcore/Prophunt-Pentagram) Allows seekers to create a trap which makes hiders slow & weak and looks like a pentagram.
- [Prophunt - Smoke](https://gitlab.com/Zipcore/Prophunt-Smoke) Allows hiders to buy smoke effect and poisonous smoke.
- [Prophunt - Taunt](https://gitlab.com/Zipcore/Prophunt-Taunt) More advaned taunt pack system which allows mixing of soundpacks.
- [Prophunt - Ultimate](https://gitlab.com/Zipcore/Prophunt-Ultimate) Allows hiders to transform into demons during endgame to hunt the seekers.